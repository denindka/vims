import { StyleSheet, Platform } from 'react-native';

const styles = StyleSheet.create({
	header: {
		height: 80,
		backgroundColor: 'rgb(7, 129, 247)',
		paddingLeft: 24,
		paddingRight: 24
	},

	icon: {
		color: '#fff',
		fontSize: 22
	},

	button: {
		position: 'relative',
		marginRight: 17
	},

	oval: {
		position: 'absolute',
		right: -6,
		top: -6,
		width: 17,
		height: 17,
		backgroundColor: 'red',
		borderRadius: 17 / 2,
		zIndex: 2
	},

	body: {
		marginRight: Platform.OS === 'ios' ? 40 : 20,
		marginLeft: Platform.OS === 'ios' ? 30 : 80
	},

	logo: { width: 53, height: 18 },

	textOval: {
		textAlign: 'center',
		color: '#fff'
	}
});

export default styles;
