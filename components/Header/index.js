import React from 'react';
import { View, Text, Image } from 'react-native';
import { Header, Left, Body, Right, Button, Icon } from 'native-base';

import styles from './styles';

const logo = require('../../assets/images/logoHeader.png');

export default () => (
	<Header style={styles.header}>
		<Left>
			<Button transparent>
				<Icon type="MaterialIcons" name="search" style={styles.icon} />
			</Button>
		</Left>
		<Body style={styles.body}>
			<Image style={styles.logo} source={logo} />
		</Body>
		<Right>
			<View style={styles.button}>
				<View style={styles.oval}>
					<Text style={styles.textOval}>3</Text>
				</View>
				<Icon type="MaterialIcons" name="chat" style={styles.icon} />
			</View>
			<View style={styles.button}>
				<View style={styles.oval}>
					<Text style={styles.textOval}>2</Text>
				</View>
				<Icon type="MaterialIcons" name="notifications" style={styles.icon} />
			</View>
			<View>
				<Icon type="MaterialIcons" name="menu" style={styles.icon} />
			</View>
		</Right>
	</Header>
);
