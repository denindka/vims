import { createSwitchNavigator } from 'react-navigation';
import LogIn from '../screens/LogIn';
import Menu from '../screens/Menu';

export default createSwitchNavigator(
	{
		// You could add another route here for authentication.
		// Read more at https://reactnavigation.org/docs/en/auth-flow.html
		LogIn: LogIn,
		Menu
	},
	{
		initialRouteName: 'LogIn'
	}
);
