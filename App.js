import React from 'react';
import { StyleSheet, View } from 'react-native';
import { AppLoading, Asset, Font } from 'expo';
import AppNavigator from './navigation/AppNavigator';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff'
	}
});

export default class App extends React.Component {
	state = {
		isLoadingComplete: false
	};

	async componentWillMount() {
		await Expo.Font.loadAsync({
			Roboto: require('native-base/Fonts/Roboto.ttf'),
			Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf')
		});
	}

	_loadResourcesAsync = async () =>
		Promise.all([
			Asset.loadAsync([
				// require('./assets/images/robot-dev.png'),
				// require('./assets/images/robot-prod.png'),
			]),
			Font.loadAsync({
				'corsicaLX-Medium': require('./assets/fonts/CorsicaLX-Medium.otf')
			})
		]);

	_handleLoadingError = error => {
		console.warn(error);
	};

	_handleFinishLoading = () => {
		this.setState({ isLoadingComplete: true });
	};

	render() {
		if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
			return (
				<AppLoading
					startAsync={this._loadResourcesAsync}
					onError={this._handleLoadingError}
					onFinish={this._handleFinishLoading}
				/>
			);
		}
		return (
			<View style={styles.container}>
				<AppNavigator />
			</View>
		);
	}
}
