import { StyleSheet, Platform } from 'react-native';

const styles = StyleSheet.create({
	item: {
		height: 52,
		marginLeft: 20,
		marginRight: 20,
		// borderLeftWidth: 4,
		// borderLeftColor: 'red',
		borderRadius: 5,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
		marginBottom: 11,
		paddingLeft: 19,
		paddingRight: 19,
		position: 'relative',
		overflow: 'hidden'
	},

	border: {
		position: 'absolute',
		left: -19,
		top: -16,
		width: 4,
		height: 56,
		backgroundColor: 'red'
	},

	leftView: {
		flexDirection: 'row',
		alignItems: 'center'
	},

	image: {
		width: 24,
		height: 24,
		marginRight: 19
	},

	icon: {
		fontSize: 18,
		marginRight: 10
	},

	label: {
		fontSize: 16,
		fontFamily: 'corsicaLX-Medium',
		marginTop: Platform.OS === 'ios' ? 5 : 0
	},

	number: {
		fontSize: 16,
		fontFamily: 'corsicaLX-Medium',
		marginTop: Platform.OS === 'ios' ? 5 : 0
	}
});

export default styles;
