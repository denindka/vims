import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
	itemList: {
		marginTop: 19
	},

	button: {
		backgroundColor: 'rgb(0, 122, 255)',
		borderRadius: 0,
		height: 57,
		justifyContent: 'center'
	},

	buttonText: {
		color: '#fff',
		fontSize: 21,
		letterSpacing: -0.54
	},

	iconQR: {
		marginRight: 12
	}
});

export default styles;
