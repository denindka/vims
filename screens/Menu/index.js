import React from 'react';
import { View, ScrollView, Text } from 'react-native';
import { Container, Button, Icon } from 'native-base';
import Header from '../../components/Header';
import Item from './Item';
import styles from './styles';

const items = [
  {
    id: 1,
    label: 'Awaiting Estimate',
    number: 16,
    color: 'rgb(5, 202, 230)',
    icon: {
      type: 'Ionicons',
      name: 'md-timer'
    }
  },
  {
    id: 2,
    label: 'Unclaimed',
    number: 16,
    color: 'rgb(67, 206, 136)',
    icon: {
      type: 'Entypo',
      name: 'briefcase'
    }
  },
  {
    id: 3,
    label: 'Work in Progress',
    number: 16,
    color: 'rgb(255, 112, 119)',
    icon: {
      type: 'MaterialIcons',
      name: 'bubble-chart'
    }
  },
  {
    id: 4,
    label: 'Awaiting Approval',
    number: 12,
    color: 'rgb(15, 132, 247)',
    icon: {
      type: 'Ionicons',
      name: 'md-timer'
    }
  },
  {
    id: 5,
    label: 'Work Approved',
    color: 'rgb(15, 132, 247)',
    number: 16,
    icon: {
      type: 'MaterialCommunityIcons',
      name: 'clipboard-check'
    }
  },
  {
    id: 6,
    label: 'Bid',
    color: 'rgb(156, 88, 203)',
    number: 10,
    icon: {
      type: 'MaterialCommunityIcons',
      name: 'gavel'
    }
  },
  {
    id: 7,
    label: 'Invoices',
    color: 'rgb(254, 200, 0)',
    number: 10,
    icon: {
      type: 'MaterialIcons',
      name: 'receipt'
    }
  },
  {
    id: 8,
    label: 'Services',
    color: 'rgb(234, 131, 177)',
    number: 12,
    icon: {
      type: 'Ionicons',
      name: 'md-clipboard'
    }
  },
  {
    id: 9,
    label: 'Completed',
    color: 'rgb(95, 48, 106)',
    number: 12,
    icon: {
      type: 'Ionicons',
      name: 'md-checkmark-circle'
    }
  }
];

export default () => (
  <Container>
    <Header />
    <ScrollView>
      <View style={styles.itemList}>
        {items.map(item => (
          <Item key={item.id} {...item} />
        ))}
      </View>
    </ScrollView>
    <Button style={styles.button} full>
      <Icon style={styles.iconQR} name="qrcode" type="FontAwesome" />
      <Text style={styles.buttonText}>Scan QR-code</Text>
    </Button>
  </Container>
);
