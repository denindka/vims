import React from 'react';
import { View, Text, Image } from 'react-native';
import { Card, Icon } from 'native-base';
import styles from './itemStyle';

const dashboard = require('../../assets/images/dashboard.png');

export default ({ label, color, number, icon: { name, type } }) => (
	<Card style={styles.item}>
		<View style={styles.leftView}>
			<Icon style={[styles.icon, { color }]} type={type} name={name} />
			<View style={[styles.border, { backgroundColor: color }]} />
			<Text style={[styles.label, { color }]}>{label}</Text>
		</View>
		<View style={styles.rigthView}>
			<Text style={[styles.number, { color }]}>{number}</Text>
		</View>
	</Card>
);
