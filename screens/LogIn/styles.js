import { StyleSheet, Dimensions } from 'react-native'

let { height } = Dimensions.get('window')

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    height,
  },

  logo: {
    width: 88,
    height: 29,
    marginTop: 72,
  },

  form: {
    marginLeft: 40,
    marginRight: 40,
    marginTop: 42,
    alignItems: 'flex-start',
  },

  label: {
    fontSize: 17,
    letterSpacing: -0.4,
    color: 'rgb(166, 173, 188)',
  },

  icon: {
    position: 'absolute',
    top: 22,
    right: -7,
    fontSize: 26,
    color: 'rgb(204, 216, 228)',
  },

  iconPerson: {
    right: -12,
  },

  input: { marginTop: 18, width: '100%' },

  item: {
    paddingLeft: 0,
    marginLeft: 0,
    position: 'relative',
  },

  button: {
    width: 335,
    height: 60,
    backgroundColor: '#268ef4',
    marginTop: 30,
    marginLeft: 20,
    marginRight: 20,
    justifyContent: 'center',
    borderRadius: 30,
    borderWidth: 2,
    borderColor: '#2184e5',
  },

  buttonText: {
    color: '#fff',
    fontSize: 21,
    letterSpacing: -0.5,
  },

  footer: {
    marginTop: 20,
    width: '100%',
    paddingLeft: 30,
    paddingRight: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  forgotPassword: {
    fontSize: 17,
    letterSpacing: -0.4,
    color: 'rgb(39, 144, 247)',
  },

  separator: {
    color: 'rgb(204, 216, 228)',
    fontWeight: '600',
    fontSize: 18,
  },

  terms: {
    fontSize: 17,
    letterSpacing: -0.4,
    color: 'rgb(166, 173, 188)',
  },
})

export default styles
