// @flow

import React, { Component } from 'react'
import { View, Text, Image, ScrollView } from 'react-native'
import { Input, Item, Label, Form, Icon, Button } from 'native-base'

import styles from './styles'

const logo = require('../../assets/images/logo.png')

class LogIn extends Component {
  state = {
    login: 'login',
    password: 'password',
  }

  render() {
    const { navigation } = this.props
    console.log(navigation)

    return (
      <View>
        <ScrollView>
          <View style={styles.container}>
            <Image source={logo} style={styles.logo} />
            <Form style={styles.form}>
              <Item floatingLabel style={styles.item}>
                <Label style={styles.label}>Enter User ID</Label>
                <Icon type="MaterialIcons" style={[styles.icon, styles.iconPerson]} name="person" />
                <Input
                  style={styles.input}
                  onChangeText={text => this.setState({ login: text })}
                  value={this.state.login}
                />
              </Item>

              <Item floatingLabel style={styles.item}>
                <Label style={styles.label}>Enter Password</Label>
                <Icon type="Ionicons" style={styles.icon} name="md-lock" />
                <Input
                  style={styles.input}
                  onChangeText={text => this.setState({ password: text })}
                  value={this.state.password}
                  secureTextEntry
                />
              </Item>
            </Form>
            <Button style={styles.button} onPress={() => navigation.navigate('Menu')}>
              <Text style={styles.buttonText}>Sign On</Text>
            </Button>
            <View style={styles.footer}>
              <Text style={styles.forgotPassword}>Forgot Password?</Text>
              <Text style={styles.separator}>|</Text>
              <Text style={styles.terms}>Terms & Conditions</Text>
            </View>
          </View>
        </ScrollView>
      </View>
    )
  }
}

export default LogIn
